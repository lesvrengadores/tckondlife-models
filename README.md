# README #

This is a repository for the 3d models made for _TCKondLife: Persons reloaded_, a VR experience to know the people working in The Cocktail. This models are intended to be used in our [Godot project](https://bitbucket.org/lesvrengadores/tckondlife-godotproject/src/master/)
All models should have a "low poly" style and be properly named!

### Contact info ###

* Project members: https://bitbucket.org/lesvrengadores/profile/members
* Project contact: fernando.garcia at the-cocktail.com